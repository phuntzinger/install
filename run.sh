command_exists() {command -v $1 > /dev/null}

run() {
      if command_exists "$1"
      then
       (echo "run $@" >&2)
       command "$1" "${@:2}"
       STATUS=$? 
       (echo "run: done [$STATUS]: $1 ${@:2}" >&2)
       return $STATUS
      fi
      (echo "run: Command not found: $1" >&2)
      return 1
}

! command_exists zsh && echo 'Default shell does not appear to be zsh' >&2 && return 1
! command_exists brew && /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
! command_exists git && brew install git
! command_exists ssh && brew install ssh

comment=$USER-$HOST-$(date +%Y%m%d)

[ ! -d "~/.ssh" ] && mkdir ~/.ssh
[ ! -f ~/.ssh/id_ed25519.pub ] && cd ~/.ssh && ssh-keygen -t ed25519 -C "$comment"
cat ~/.ssh/id_ed25519.pub|pbcopy # this cmd is only for Mac

echo 'Goto https://gitlab.com/-/profile/keys and paste the public key in Key area.'
open https://gitlab.com/-/profile/keys # Mac command
echo 'Press any key to continue...'; read -k1 -s

ssh-add -l||ssh-add -K ~/.ssh/*
ssh -T git@gitlab.com || return

[ ! -d ~/personal_settings ] && run git clone git@gitlab.com:phuntzinger/personal_settings.git ~/personal_settings
run ~/personal_settings/bin/installer.sh
